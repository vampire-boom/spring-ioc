package com.ssm;

import com.spring.IComponentScan;

/**
 * @author ssm
 * @date 2020/11/26 12:55
 * 包扫描路径
 */
@IComponentScan("com.ssm.service")
public class AppConfig {
}
