package com.ssm.service;

import com.spring.*;

/**
 * @author ssm
 * @date 2020/11/26 13:41
 */
@IComponent("orderService")
@Scope("prototype")
public class OrderService implements InitializingBean, BeanNameAware {

    @IAutowired
    private UserService userService;

    //这个beanName主要想要实现spring能够自动将OrderService这个Bean的名字（即Component里面的名字）赋值给这个beanName
    private String beanName;

    public void test(){
        //这里测试是看输出的这个userservice是否为空，如果不为空就说明我们写的依赖注入功能实现了
        System.out.println(userService);
    }

    @Override
    public void afterPropertiesSet() {
        //bean在生成的过程当中会调用这个方法
        System.out.println("初始化");
    }

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
        //测试是否在生成bean时候自动赋值了beanName
        System.out.println("beanName:  "+beanName);
    }
}
