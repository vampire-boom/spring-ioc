package com.ssm.service;

import com.spring.BeanPostProcessor;
import com.spring.IComponent;

/**
 * @author ssm
 * @date 2020/11/27 19:28
 */
@IComponent
public class SSMBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        System.out.println("初始化前  ");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println("初始化后  ");
        return bean;
    }
}
