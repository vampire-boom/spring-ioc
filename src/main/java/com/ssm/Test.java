package com.ssm;

import com.spring.SSMApplicationContext;
import com.ssm.service.OrderService;

/**
 * @author ssm
 * @date 2020/11/26 12:50
 */
public class Test {
    public static void main(String[] args) {
        //启动spring(启动了以后要加载配置，所以需要传入配置，而配置类主要是启动了包扫描的作用)
        SSMApplicationContext ssmApplicationContext = new SSMApplicationContext(AppConfig.class);

        //getBean()
        OrderService orderservice = (OrderService) ssmApplicationContext.getBean("orderService");
        System.out.println(orderservice);
        orderservice.test();//验证依赖注入的功能是否实现

        //测试单例singleton和prototype
//        System.out.println(ssmApplicationContext.getBean("orderService"));
//        System.out.println(ssmApplicationContext.getBean("orderService"));
//        System.out.println(ssmApplicationContext.getBean("orderService"));

        //测试BeanPostProcessor
//        System.out.println(ssmApplicationContext.getBean("orderService"));
    }
}
