package com.spring;

/**
 * @author ssm
 * @date 2020/11/27 19:22
 */
public interface BeanPostProcessor {
    //bean初始化方法调用前被调用
    Object postProcessBeforeInitialization(Object bean,String beanName);
    //bean初始化方法调用后被调用
    Object postProcessAfterInitialization(Object bean,String beanName);
}
